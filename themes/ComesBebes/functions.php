<?php
function wpdocs_theme_name_styles() {
    wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css' );

    if ( is_home()){
        wp_enqueue_style( 'startPage', get_template_directory_uri() . '/style/startPage.css' ); 
    }
    if ( is_single() ){
        wp_enqueue_style( 'singleProductPage', get_template_directory_uri() . '/style/single-product.css');
    }
}


function enqueue_js_scripts(){
    wp_enqueue_script( 'open-close-cart', get_template_directory_uri() . '/js/openAndCloseCart.js');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_styles' );
add_action( 'wp_enqueue_scripts', 'enqueue_js_scripts');

add_theme_support( 'widgets' );
add_theme_support( 'menus' );
add_theme_support( 'woocommerce');

function get_dish_categories(){
    $pratos = get_term_by('slug', 'pratos', 'product_cat');	
    $pratos = $pratos->to_array();
    $prod_cat_args = array(
        'taxonomy'    => 'product_cat',//Especifica o que queremos buscar
        'orderby'     => 'id',
        'hide_empty'  => false,
            'parent'      => $pratos['term_id']
    );

    $woo_categories = get_categories( $prod_cat_args );
    echo '<div class="category-wrapper">';
    foreach ( $woo_categories as $woo_cat ) {
        $woo_cat_id = $woo_cat->term_id;
        $woo_cat_name = $woo_cat->name;
        $woo_cat_slug = $woo_cat->slug;
        $category_thumbnail_id = get_term_meta($woo_cat_id, 'thumbnail_id', true);
        $thumbnail_image_url = wp_get_attachment_image_src( $category_thumbnail_id)[0];
        echo '<div class="category-dish">';
        echo '<img src="' . $thumbnail_image_url . '">';  
        echo '<a href="' . get_term_link( $woo_cat_id, 'product_cat' ) . '"> <span class="name-background"><h4 class="category-name">' . strtoupper($woo_cat_name) . '</h4></span></a>';
        echo "</div>\n";
    }
    echo '</div>';
}

function get_dish_categories_shop_page(){
    $pratos = get_term_by('slug', 'pratos', 'product_cat');	
    $pratos = $pratos->to_array();
    $prod_cat_args = array(
        'taxonomy'    => 'product_cat',//Especifica o que queremos buscar
        'orderby'     => 'id',
        'hide_empty'  => false,
            'parent'      => $pratos['term_id']
    );

    $woo_categories = get_categories( $prod_cat_args );
    echo '<h2 style="font-size: 1.7em">SELECIONE UMA CATEGORIA</h2>';
    echo '<div class="category-wrapper">';
    foreach ( $woo_categories as $woo_cat ) {
        $woo_cat_id = $woo_cat->term_id;
        $woo_cat_name = $woo_cat->name;
        $woo_cat_slug = $woo_cat->slug;
        $category_thumbnail_id = get_term_meta($woo_cat_id, 'thumbnail_id', true);
        $thumbnail_image_url = wp_get_attachment_image_src( $category_thumbnail_id)[0];
        echo '<div class="category-dish">';
        echo '<img src="' . $thumbnail_image_url . '">';  
        echo '<a href="' . get_term_link( $woo_cat_id, 'product_cat' ) . '"> <span class="name-background"><h4 class="category-name">' . strtoupper($woo_cat_name) . '</h4></span></a>';
        echo "</div>\n";
    }
    echo '</div>';
}

function get_product_description(){
    global $product;
    echo '<p class="product-description">' . $product->description . '</p>';
}

function thumbnail_background_div(){
    global $product;
    $dish_image_url = wp_get_attachment_image_src($product->image_id, [200,200])[0];
    echo '<div class="product-wrapper" style="background-image: url(' . $dish_image_url . ');">';
}

function create_shop_container_div(){
    echo '<div class="shop-container">';
}

function create_search_filters_wrapper(){
    echo '<div class="search-filters-wrapper">';
}

function close_div(){
    echo '</div>';
}

function add_to_cart_button(){
     echo '<input type="image" class="dish-button" src="' . get_stylesheet_directory_uri() . '/img/AddToCartButton.png"></button>';
}

function searchbar(){
    
    get_product_search_form();

}

function edit_account_form(){
    ?>

    <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
    <div class="edit-form-name-area">
	    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		    <label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />
	    </p>
	    <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		    <label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
	    </p>
    </div>
	<div class="clear"></div>

	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</p>

	<fieldset>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'SAVE CHANGES', 'woocommerce' ); ?>"><?php esc_html_e( 'SAVE CHANGES', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

    <script>splitLink = window.location.href.split('/');
            if(splitLink[4] != ""){
                //Caso a página não seja a my-account, esconda o formulário
                form = document.querySelector('.woocommerce-EditAccountForm');
                form.style.display = 'none';
            }        
            else{
                form = document.querySelector('.woocommerce-EditAccountForm');
                form.style.display = 'flex';
            }
    </script>
<?php ;}

add_action( 'woocommerce_account_content', 'edit_account_form');

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);

add_action( 'woocommerce_before_shop_loop_item_title', 'thumbnail_background_div', 10);

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

add_action( 'woocommerce_after_shop_loop_item', 'add_to_cart_button', 3);

add_action( 'woocommerce_before_shop_loop', 'create_shop_container_div', 3);
add_action( 'woocommerce_before_shop_loop', 'get_dish_categories_shop_page', 10);
add_action( 'woocommerce_before_shop_loop', 'create_search_filters_wrapper', 15);
add_action( 'woocommerce_before_shop_loop', 'searchbar', 20);
add_action( 'woocommerce_before_shop_loop', 'close_div', 40);

remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action( 'woocommerce_single_product_summary', 'get_product_description');

add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 10);
?>