<?php 
//Template Name: Home
?>

<?php get_header();?>
<section class="title-area">
    <h1 class="title-text">Comes&Bebes</h1>
    <p>O restaurante para todas as fomes</p>
</section>
<section class="dishes-area">
    <h2>CONHEÇA NOSSA LOJA</h2>
    <div class="main-dishes-area">
        <p>Tipos de pratos principais</p>
        <div class="container-main-dishes">
            <!--Diferentes categorias de pratos.-->
        <?php
            get_dish_categories();
        ?>
        </div>
    </div>
    <div class="day-dish-area">
        <p>Pratos do dia de hoje:</p>
        <p>SEGUNDA</p>
        <div class="container-day-dishes">
            <?php 
                /*date_default_timezone_set('America/Sao_Paulo');
                $weekday = date('l');
                echo $weekday;
                Por enquanto, usaremos só segunda
                */
                function get_dishes_by_weekday(string $weekday) {
                    $args = array(
                        'product_tag' => $weekday,
                        'post_type' => 'product',
                        'posts_per_page' => 4,
                    );

                    $day_dishes = new WP_Query($args);
                    
                    $day_dishes = $day_dishes->get_posts();

                    foreach($day_dishes as $dish){
                        $wc_dish = wc_get_product($dish->ID);
                        $dish_image_id = $wc_dish->get_image_id();
                        //Procura imagens com alturas grandes para as imagens de produto
                        //certas proporções causam repetção nessa configuração
                        //Procurar alternativa caso dê tempo
                        $dish_image_src = wp_get_attachment_image_src($dish_image_id, [200,200]);
                        $dish_name = $wc_dish ->get_name();
                        $dish_price = $wc_dish->get_price_html();
                        
                        echo '<div class="dish-image" style="background-image: url(' . $dish_image_src[0]. ');"/>';

                            echo '<div class="dish-info-background">';

                                echo '<div class="container-dish-info">';
                                    echo '<p class="dish-name">' . $dish_name . '</p>';
                                    echo '<p class="dish-price">' . $dish_price . '</p>';
                                echo '</div>';

                            echo '<input type="image" class="dish-button" src="' . get_stylesheet_directory_uri() . '/img/AddToCartButton.png" onclick="window.location.href =' . "'" . get_permalink($dish->ID) . "'" . '"></button>';

                            echo '</div>';

                        echo '</div>';
                    }

                }
                $weekday = "Monday";
                if($weekday == "Monday"){
                    get_dishes_by_weekday('segunda');
                }
            ?>
        </div>
    </div>
    <button class="options-button" onclick="window.location.href = 'http://tarefawordpress.local/shop/'">Veja outras opções</button>
</section>
<section class="phys-store-area">
    <h2>VISITE NOSSA LOJA FÍSICA</h2>
    <div class="items-wrapper">
        <div class="container-contact">
            <iframe class="iframe-IC" style="border:0" loading="lazy" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJK2lORH6BmQAR9fJ6V_s1Xv0&key=AIzaSyBjqMWIiZ_yM8GRPzFPSzIdcqjHcV6RbtE"></iframe>
            <p><?php echo get_option( 'woocommerce_store_address' );?></p>
            <p>(XX) XXXX-XXXX</p>
        </div>

        <!--PHP para pegar a imagem da base de dados-->
        <?php
            $args = array(
                'post_type' => 'post',
                'name' => 'items-slide',
            );

            $slide_gallery_post = new WP_Query($args);
            $slide_gallery_post = $slide_gallery_post->get_posts()[0];

            $galleries =  get_post_galleries( $slide_gallery_post->ID, false );

            $image_ids = array();
            foreach ($galleries as $gallery){
                if ( ! empty ( $gallery[ 'ids' ] ) ) {
                    $image_ids = array_merge( $image_ids, explode( ',', $gallery[ 'ids' ] ) );
                }
            }
            $image_ids = array_unique( $image_ids );
            $image_urls = array_map( "wp_get_attachment_url", $image_ids );

            foreach($image_urls as $image_url){
                echo '<img src="' . $image_url . '"/>' . "\n";
            }
        ?>
    </div>
</section>
<?php get_footer();?>