function openCart(){
    const cart = document.querySelector('.cart-menu');
    cart.style.display = 'flex';
}

function closeCart(){
    const cart = document.querySelector('.cart-menu');
    cart.style.display = 'none';
}