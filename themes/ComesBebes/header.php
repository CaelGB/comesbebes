<!DOCTYPE html>
<html>
    <head>
        <title>Comes e Bebes</title>
        <meta name="Comes e bebes" content="Pagina do Restaurante Comes e Bebes" charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Bellota+Text&family=Roboto&display=swap" rel="stylesheet"> 

        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/reset.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/style.css">
        <?php wp_head();?>
    </head>
    <body>
        <header class="header-container">
            <div class="left-container">
                <a href="http://tarefawordpress.local/"><img src="<?php echo get_stylesheet_directory_uri()?>/img/VectorLogo.png"></a>
                <div class="wrapper-searchbox">
                    <?php searchbar(); ?>
                </div>
            </div>
            <div class="right-container">
                <button class="make-order-button" onClick="window.location = 'http://tarefawordpress.local/shop/'">Faça um pedido</button>
                <button class="cart-button" onClick="openCart()"><img src="<?php echo get_stylesheet_directory_uri()?>/img/VectorCart.png"></button>
                <a href="http://tarefawordpress.local/my-account/"><img src="<?php echo get_stylesheet_directory_uri()?>/img/VectorProfile.png"></a>
            </div>
        </header>
        <section class="cart-menu">
            <button class="close-cart" onclick="closeCart()"></button>
            <div class="cart-container">
                <h1>CARRINHO</h1>
                <div class="cart-contents">
                    <?php
                        $cart = WC()->cart->get_cart();
                        foreach ( $cart as $cart_item_key => $cart_item ){
                            echo '<div class="cart-item">';
                            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                $item_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( $cart_item['data']->id ), 'single-post-thumbnail' )[0];
                                echo '<div class="cart-item-thumbnail" style="background-image: url(' . $item_thumb_url . ');"></div>';
                                echo '<div class="cart-item-info">';
                                    echo '<h3>' . $cart_item['data']->name . '</h3>';
                                    $product_quantity = woocommerce_quantity_input(
                                        array(
                                            'input_name'   => "cart[{$cart_item_key}][qty]",
                                            'input_value'  => $cart_item['quantity'],
                                            'max_value'    => $_product->get_max_purchase_quantity(),
                                            'min_value'    => '0',
                                            'product_name' => $_product->get_name(),
                                        ),
                                        $_product,
                                        false
                                    );

                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                                echo '<p>' . $cart_item[ 'data' ]->price . '</p>';
                                echo '</div>';
                            echo '</div>';
                        }
                    ?>
                </div>
                <p>Total do carrinho: R$<?php echo WC()->cart->get_cart_contents_total()?></p>
                <button class="buy-button" onClick="window.location = 'http://tarefawordpress.local/checkout/'">COMPRAR</button>
            </div>
        </section>
